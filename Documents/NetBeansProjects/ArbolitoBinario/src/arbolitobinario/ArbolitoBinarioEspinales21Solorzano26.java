package arbolitobinario;

import javax.swing.JOptionPane;
//Trabajaremos con 3 clases, la clase NodoArbol se compone de la variable dato 
//de tipo entero, dos punteros de tipo NodoArbol los cuales son nodoDere y nodoIzq

class NodoArbol {

    int dato;
    NodoArbol nodoIzq, nodoDere;

    //se crea el constructor de la clase y recibe de parametro la variable dat
    //de tipo entero
    public NodoArbol(int dat) {
        //se inicializan las variables
        this.dato = dat;
        this.nodoDere = null;
        this.nodoIzq = null;

    }

    //se implementa el metodo que nos permitirá mostrar el valor de todos los atributos que desees
    public String toString() {
        //se concatena la variable con una cadena mas la variable dato 
        return "su dato es " + dato;
    }
}
//Se crea la clase ArbolBinario en esta clase se implementarán todos los metodos de acuerdo a las operciones
//permitidas por el TAD Lista 

class ArbolBinario {

    //Se crea la varible de tipo NodoArbol llamada raiz
    NodoArbol raiz;

    //se crea el constructor para inicializar la raiz en null
    public ArbolBinario() {
        raiz = null;
    }

    //Se crea el metodo para insertar un nodo en el arbol
    public void insertarNodo(int dat) {
        //se crea una sentencia new con la varible nuevo de tipo NodoArbol
        NodoArbol nuevo = new NodoArbol(dat);
        //se crea lacondicion de si raiz es igual a null quiere decir que el arbol está vacío
        if (raiz == null) {
            //se ingresa el pimer elemento al arbol que viene a ser la raiz
            raiz = nuevo;
            //en caso de no estar vacío el arbol se toman en cuenta las siguientes condiciones
        } else {
            //se crea una variable JavierAlisson de tipo NodoArbol que apunte a la raiz
            NodoArbol JavierAlisson = raiz;
            //se crea otra variable de tipo NodoArbol llamada Javier
            NodoArbol Javier;
            //se crea un ciclo, mientras sea verdadero 
            while (true) {
                //Javier apunta a JavierAlisson
                Javier = JavierAlisson;
                //se crea la condicion si dat es menor que JavierAlisson.dato
                if (dat < JavierAlisson.dato) {
                    //JavierAlisson apunta a auxiliar de nodoIq
                    JavierAlisson = JavierAlisson.nodoIzq;
                    //se crea otra condicion que pregunta si JavierAlisson es igual a null significa que llegó al final
                    if (JavierAlisson == null) {
                        //quiere decir que es allí donde se va a insertar
                        Javier.nodoIzq = nuevo;
                        //se crea un return para finalizar la ejecución del método esto en el caso de que dat sea menor 
                        //que JavierAlisson de dato
                        return;
                    }
//en caso de no ser menor significa que es mayor o igual y se coloca hacia la derecha
                } else {
                    JavierAlisson = JavierAlisson.nodoDere;
                    //se crea la siguiente condicion
                    if (JavierAlisson == null) {
                        //en caso de que ya no exista mas lugar donde colocar el dato se coloca al final simepre 
                        //y cuando esté en orden
                        Javier.nodoDere = nuevo;
                        return;
                    }
                }
            }
        }
    }
//se crea un metodo pequeño para saber si el arbol está vacío
    public boolean Vacio() {
        return raiz == null;
    }
//se crea un metodo que nos permite ordenar el arbol en modo inOrden
    public void HacerinOrden(NodoArbol r) {
        if (r != null) {
            HacerinOrden(r.nodoIzq); //Izquierda
            System.out.print(r.dato + ", "); //Raiz
            HacerinOrden(r.nodoDere); //Derecha
        }

    }
//se crea un metodo que nos permite ordenar el arbol en modo preOrden
    public void HacerpreOrden(NodoArbol r) {
        if (r != null) {
            System.out.print(r.dato + ", "); //Raiz
            HacerpreOrden(r.nodoIzq); //Izquierda
            HacerpreOrden(r.nodoDere); //Derecha
        }
    }
//se crea un metodo que nos permite ordenar el arbol en modo postOrden
    public void HacerpostOrden(NodoArbol r) {
        if (r != null) {
            HacerpostOrden(r.nodoIzq); //Izquierda
            HacerpostOrden(r.nodoDere); //Derecha
            System.out.print(r.dato + ", "); //Raiz
        }
    }
//se crea un metodo que nos permite recorrer el arbol en busca de un nodo
    public NodoArbol BuscarNodo(int d) {
        //creamos una variable aux de tipo NodoArbol que apunte a raiz
        NodoArbol aux = raiz;
        //se crea un ciclo que nos dice que mientras aux.dato sea diferente de d (el dato que se está buscando)
        while (aux.dato != d) {
            //se crean las siguientes condiciones para recorrer el ciclo
            if (d < aux.dato) {
                aux = aux.nodoIzq;
            } else {
                aux = aux.nodoDere;
            }
            //se crea la condicion que nos indica que llego al final y que no encontro nada
            if (aux == null) {
                return null;
            }
        }
        //se retorna aux que es el dato que estamos buscando
        return aux;
    }
//Se crea el metodo EliminarNodo que nos permite eliminar un nodo sin perder el orden del arbol como debería ser
    public boolean EliminarNodo(int d) {
        //los dos punteros que son JavierAlisson y Javier a raiz
        NodoArbol JavierAlisson = raiz;
        NodoArbol Javier = raiz;
        // se  inicializa un boleano NodoIzquierdo que apunta a true es decir que si es izquierdo va a estar adentro si no al false
        boolean NodoIzquierdo = true;
       //mientras  JavierAlisson sea diferente o igual que d, quiere decir que no lo encuentra
        while (JavierAlisson.dato != d) {
            Javier = JavierAlisson;
            // quiere decir que se debe ir por la izquierda 
            if (d < JavierAlisson.dato) {
                
                NodoIzquierdo = true;
                //esto es recorriendo por la izquierda por es menor 
                JavierAlisson = JavierAlisson.nodoIzq;
            } else {
                // si es falso es obio que es mayor 
                NodoIzquierdo = false;
                //por lo que se va a la derecha 
                JavierAlisson = JavierAlisson.nodoDere;
            }
            if (JavierAlisson == null) {
                //metodo para ver que no existe y termina este while
                return false;
                
            }

        }//fin del while
        
        //aqui quiere decir que es vacio o que es solo unicamente a la raiz 
        if (JavierAlisson.nodoIzq == null && JavierAlisson.nodoDere == null) {
            //es la raiz 
            if (JavierAlisson == raiz) {
                // queremos eliminar la raiz 
                raiz = null;
            } else if (NodoIzquierdo) {
               //
                Javier.nodoIzq = null;
               //  si no entra a esas condiciones 
            } else {
                Javier.nodoDere = null;
            }
           // 
        } else if (JavierAlisson.nodoDere == null) {
            if (JavierAlisson == raiz) {
               
                raiz = JavierAlisson.nodoIzq;
            } else if (NodoIzquierdo) {
                Javier.nodoIzq = JavierAlisson.nodoIzq;

            } else {
                Javier.nodoDere = JavierAlisson.nodoIzq;
            }
        } else if (JavierAlisson.nodoIzq == null) {
            if (JavierAlisson == raiz) {
                raiz = JavierAlisson.nodoDere;
            } else if (NodoIzquierdo) {
                Javier.nodoIzq = JavierAlisson.nodoDere;

            } else {
                Javier.nodoDere = JavierAlisson.nodoIzq;
            }
        } else {
          // se agrega una variable alisson que practicamente es para reemplazar  
            NodoArbol Alisson = obtenerNodoAlisson(JavierAlisson);
            if (JavierAlisson == raiz) {
                //para este caso a raiz se reemplaza 
                raiz = Alisson;
            } else if (NodoIzquierdo) {
               //para este el nodoIzq
                Javier.nodoIzq = Alisson;
            } else {
                //para este el nodoDere
                Javier.nodoDere = Alisson;
            }
            Alisson.nodoIzq = JavierAlisson.nodoIzq;
        }
        //quiere decir que si encontro el nodo que se desea elimina 
        return true;

    }
//Metodo encargado de devolvernos el nodo reemplazo 
    public NodoArbol obtenerNodoAlisson(NodoArbol nodoAlis) {
        //se creean 3 punteros de tipo NodoArbol 
        NodoArbol AlissonJavier = nodoAlis;
        NodoArbol Alisson = nodoAlis;
        NodoArbol JavierAlisson = nodoAlis.nodoDere;
        //se crea un ciclo while para encontrar cual es el nodo que reemplazara al que se va a eliminar
        while (JavierAlisson != null) {
            AlissonJavier = Alisson;
            Alisson = JavierAlisson;
            JavierAlisson = JavierAlisson.nodoIzq;
        }
        //se crea una validacion que nos dice si Alisson es diferente de nodoAlis.nodoDere
        //esto servirá para ordenar los nodos
        if (Alisson != nodoAlis.nodoDere) {
            AlissonJavier.nodoIzq = Alisson.nodoDere;
            Alisson.nodoDere = nodoAlis.nodoDere;
        }
        System.out.println("Nodo reemplazo es " + Alisson);
        return Alisson;

    }
}
//La clase madre del proyecto
public class ArbolitoBinarioEspinales21Solorzano26 {

    /**
     * @param args the command line arguments
     */
    //Se crea la clase main donde se ejecuta todo el proyecto
    public static void main(String[] args) {
        // se crean 2 variables de tipo entero 
        int opcion = 0, elemento;
        //Se crea una sentencia new que nos permitirá llamar a los objetos de la clase ArbolBinario
        ArbolBinario arbol = new ArbolBinario();
        //se crea el menu para acceder a los metodos desde una interfaz
        do {
            try {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, "1. Agregar un Nodo\n"
                        + "2. Recorrer el arbol en inOrden\n"
                        + "3. Recorrer el arbol en preOrden\n"
                        + "4. Recorrer el arbol en postOrden\n"
                        + "5. Buscar Nodo en el Arbol\n"
                        + "6. Eliminar Nodo en el Arbol\n"
                        + "7. Salir\n" + "Elige una opcion"));
        //se crea el switch para agilizar la toma de decisiones multiples en este caso las opciones del menú        
                switch (opcion) {
                    //Opcion 1 nos permite ingresar un dato por teclado que viene a ser el dato que queremos ingresar
                    case 1:
                        elemento = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresa el Numero del Nodo"));
                        arbol.insertarNodo(elemento);
                        break;
                    //Opcion 2 nos permite ordenar el arbol en modo inOrden    
                    case 2:
                        if (!arbol.Vacio()) {
                            System.out.println();
                            arbol.HacerinOrden(arbol.raiz);
                        } else {
                            JOptionPane.showMessageDialog(null, "El Arbol esta vacio");
                        }
                        break;
                    //Opcion 3 nos permite ordenar el arbol en modo preOrden     
                    case 3:
                        if (!arbol.Vacio()) {
                            System.out.println();
                            arbol.HacerpreOrden(arbol.raiz);
                        } else {
                            JOptionPane.showMessageDialog(null, "El Arbol esta vacio");
                        }
                        break;
                    //Opcion 4 nos permite ordenar el arbol en modo postOrden 
                    case 4:
                        if (!arbol.Vacio()) {
                            System.out.println();
                            arbol.HacerpostOrden(arbol.raiz);
                        } else {
                            JOptionPane.showMessageDialog(null, "El Arbol esta vacio");
                        }
                        break;
                    //Opcion 5 nos permite ingresar un dato por teclado y buscarlo en el arbol retornando el dato si ha sido encontrado 
                        //caso contrario retorna un mensaje que dice Nodo no encontrado
                    case 5:
                        if (!arbol.Vacio()) {
                            elemento = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresa el Numero a buscar"));

                            if (arbol.BuscarNodo(elemento) == null) {
                                JOptionPane.showMessageDialog(null, "Nodo no encontrado");
                            } else {
                                System.out.println("Nodo encontrado " + arbol.BuscarNodo(elemento));
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "El Arbol esta vacio");
                        }
                        break;
                    //Opcion 6 nos permite ingresar un dato por teclado siendo este eliminado del arbol en caso de que se encuentre en el arbol
                    case 6:
                        if (!arbol.Vacio()) {
                            elemento = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresa el Numero a eliminar"));

                            if (arbol.EliminarNodo(elemento) == false) {
                                JOptionPane.showMessageDialog(null, "Nodo no encontrado");
                            } else {
                                JOptionPane.showMessageDialog(null, "El nodo ha sido eliminado correctamente");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "El Arbol esta vacio");
                        }
                        break;
                    //Opcion 7 nos permite salir de la interfaz y finalizarla
                    case 7:
                        JOptionPane.showMessageDialog(null, "Aplicacion Finalizada");
                        break;
//si el numero que ingresamos es mayor de 7 nos muestra un mensaje que diga opcion incorrecta
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion incorrecta");
                }
                //en caso de que querramos cerrarla sin la opcion 7 nos muestra un mensaje de error
            } catch (NumberFormatException n) {
                JOptionPane.showMessageDialog(null, "Error " + n.getMessage());
            }
        } while (opcion != 7);
    }

}
